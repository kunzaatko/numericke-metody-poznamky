# Poznámky z numerických metod
## Content
Kapitoly obsažené v tomto dokumentu jsou následující: *Reprezentace čísel v počítači*, *Chyby*, *Úlohy lineární algebry*, *Aproximace funkcí a interpolace*, *Řešení nelineárních rovnic*, *Hledání extrémů funkcí*, *Numerická integrace* a *Obyčejné diferenciální rovnice*.

### Missing Content
  * Proč jsou bazické funkce v Lagrangeově polynomu takové, jaké jsou (odvození někde v materiálech Váchala).
  * U Čebyševových metod je důležité zdůraznit, že se musí vždy transformovat interval na $`(-1, 1)`$ - transformaci lze odvodit nalezením přímky procházející body $`<a, -1>`$ a $`<b, 1>`$.
  * Transformovat se musí také u Gaussových kvadratur - u Gaussovy kvadratury stačí znát, jak se to používá, odvození se nežádá, jen třeba vědět, proč jsou tak významné (přesnost a jednoduchost použití).
  * Automatická volba kroku u ODR - chybí, může být ve zkoušce.
  * Výhody a nevýhody složených pravidel a složená pravidla obecně - typicky se bere jednoduchý vzorec (midpoint nebo lichoběžníková metoda) a aplikujeme na $`N`$ uzlů, kde chceme funkci integrovat.
  * Není rozebrána Van der Moondova matice, která je typickým příkladem špatně podmíněné matice. Tragédie nastává u ekvidistantních bodů Lagrangeova polynomu.
  * Z Nevilleova pravidla získáme přesnou hodnotu Lagrangeova polynomu, ale je výpočetně poměrně drahý! Také z něj nezískáme konkrétní tvar polynomu.
  * Zvonový spline

## General
Toto je shrnutí látky předmětu *Numerické metody 1* (12NME01), který je na *Fakultě jaderné a fyzikálně inženýrské* pro obor *matematické inženýrství* při zaměření *apikované matematicko-stochastické metody* (MI - AMSM) přednášen v letním semestru druhého ročníku [*profesorem Limpouchem*](http://kfe.fjfi.cvut.cz/~limpouch/) a [*doktorem Váchalem*](http://kfe.fjfi.cvut.cz/~vachal/). Tento dokument vychází z přednášek v roce 2020, které proběhly *distančně*, formou posílání materiálů k předmětu přednášejícími. Materiály, ze kterých tedy tento přehled vychází jsou dostupné na stránkách přednášejících.

Toto je upravený přepis výpisků *Tadeáše Němce*, které s námi během zkouškového sdílel. Proto za všechny hezky vysvětlené pojmy a pedagogicky okomentované důkazy vděčí tento dokument jemu. Za všechny nepřesnosti, nepřehlednost a chyby jsem zodpovědný já.

Předmět je doplněn *cvičením*, které se zbývalo *implementací metod* vyložených na přednášce. Na výběr byly dva jazyky, *Python* a *Matlab*. Veškeré mé implementace ze cvik z toho předmětu (nejprve v Matlabu a následně v Pythonu) s doprovodnou prezentací od [cvičícího doktoranda Ing. Tomáše Kerepeckého](https://www.utia.cas.cz/people/kerepeck) jsou [zde](https://gitlab.com/kunzaatko/nme01).

## Contribution
Uvítám jakékoliv opravy. Pokud naleznete chybu, nepřesnost, nepřehlednost a chcete s tím něco udělat (což by bylo super), máte 3 možnosti:
  1) opravit sami a podat *pull request*, který samozřejmě, pokud nebude vážný důvod to neudělat, přijmu (**pokud možno upřednostňuji tuto variantu**)
  2) přidat jako issue a přidělit mě k němu a já je časem opravím
  3) napsat mi [e-mail](mailto:kunzmart@fjfi.cvut.cz)

U posledních dvou variant, prosím, pište přesně, kde je chyba a jak by to mělo být.

Dále uvítám jakékoliv příspěvky v podobě nových kapitol, nebo doplnění, upřesnění formulace atd. V tomto případě využijte jednu z prvních dvou možností.

## Goals
Co bych ještě časem dodělal, až na to budu mít čas a chuť (kdo chce tak se do toho může pustit - jsou na to vytvořené issues)
  1) obrázky (ze cvičení, kde jsou z Matplotlib grafy metod)
  2) zvonový spline

## License
V případě potřeby je tento dokument licencován pod
  * [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
  * [MIT license](http://opensource.org/licenses/MIT)

podle vaší volby.

## Authors
Z původních poznámek *Tadeáše Němce* ([nemectad@fjfi.cvut.cz](mailto:nemectad@fjfi.cvut.cz)), přepsal *Martin Kunz* ([kunzmart@fjfi.cvut.cz](mailto:kunzmart@fjfi.cvut.cz)).
